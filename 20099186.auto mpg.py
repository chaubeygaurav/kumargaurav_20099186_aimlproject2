#!/usr/bin/env python
# coding: utf-8

# ## Auto Mpg 

# #### Importing the necessary libraries

# In[1]:


import pandas as pd
import numpy as np
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from scipy import stats
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import RidgeCV
from sklearn.pipeline import make_pipeline
from sklearn import preprocessing
from sklearn.model_selection import train_test_split


# In[2]:


df= pd.read_csv('auto-mpg.csv')
df.head()


# In[3]:


df.isnull().sum()


# In[4]:


df.describe()


# In[5]:


df.info()


# ### Training and Testing the dataset

# In[6]:


features = ['weight', 'model year', 'origin']


# In[7]:


X = df[features]
y = df['mpg']


# In[8]:


# Train Test Split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42)


# In[9]:


# Initiate mode
lr = LinearRegression()


# In[10]:


# Fit model
lr.fit(X_train, y_train)


# In[11]:


# Get predictions
test_predict = lr.predict(X_test)
train_predict = lr.predict(X_train)
from sklearn.metrics import r2_score


# In[12]:


# Score and compare
print(r2_score(y_test, test_predict))
print(r2_score(y_train, train_predict))


# In[13]:


plt.scatter(test_predict, y_test)


# 

# In[ ]:




