#!/usr/bin/env python
# coding: utf-8

# ## Haberman Survival Analysis 

# #### Importing the necessary libraries

# In[1]:


import pandas as pd
import numpy as np
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.neighbors import KNeighborsClassifier
from matplotlib import pyplot as plt


# In[2]:


df = pd.read_csv('haberman original.csv')
df.head()


# In[15]:


df.describe()


# In[16]:


df.info()


# In[18]:


df.shape


# In[19]:


df.isnull().sum()


# #### Training and Testing the data

# In[7]:


X = df[["age", "patient's year of operation", "num. pos axillary nodes detected"]].values
y = df["survival status (1=survived 5+ years; 2=died within 5 years)"].values

# Split dataset into train and test data
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2 , random_state=191)


# #### Finding k

# In[8]:


k_values = range(1, 200 + 1)
cv_scores = []


# In[9]:


for k in k_values:
    # Create KNN classifier
    knn = KNeighborsClassifier(n_neighbors=k)


# In[10]:


# Fit the classifier to the data
knn.fit(X_train, y_train)


# In[11]:


# we are  Performing  cross validation here
scores = cross_val_score(knn, X_train, y_train, cv=10, scoring='accuracy')


# In[14]:


# Add to cv_scores
cv_scores.append(scores.mean())


# In[13]:


# Finding the  best K
best_cv_score = max(cv_scores)
best_k = k_values[cv_scores.index(best_cv_score)]
print(f"Optimal K: {best_k} with {best_cv_score}")


# In[6]:


# Plotting the  graph
plt.plot(k_values, cv_scores)
plt.xlabel("Number of Neighbors K")
plt.ylabel("Cross-validation Score")
plt.savefig("graph.png", dpi=150)
plt.show()


# In[ ]:





# In[ ]:




